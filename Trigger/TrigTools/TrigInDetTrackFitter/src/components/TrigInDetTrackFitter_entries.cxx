#include "TrigInDetTrackFitter/TrigInDetTrackFitter.h"
#include "TrigInDetTrackFitter/TrigInDetBremDetectionTool.h"
#include "TrigInDetTrackFitter/TrigDkfTrackMakerTool.h"
#include "TrigInDetTrackFitter/TrigL2ResidualCalculator.h"
#include "TrigInDetTrackFitter/TrigL2FastExtrapolationTool.h"


DECLARE_COMPONENT( TrigInDetTrackFitter )
DECLARE_COMPONENT( TrigInDetBremDetectionTool )
DECLARE_COMPONENT( TrigDkfTrackMakerTool )
DECLARE_COMPONENT( TrigL2ResidualCalculator )
DECLARE_COMPONENT( TrigL2FastExtrapolationTool )

